package lambdaandstreams.demo2;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Employee {
    private String firstName;
    private String lastName;
    private int yearsOfService;
    private Employee manager;
    private int numberOfDirectReports;
}
