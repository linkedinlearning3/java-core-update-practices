package lambdaandstreams.streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        List<String> shoppingList = new ArrayList<>();
        shoppingList.add("cOffee");
        shoppingList.add("caKe");
        shoppingList.add("bRead");
        shoppingList.add("milk");

        Stream<String> stream = shoppingList.stream();
        stream.sorted()
                .map(item -> {
                    if (item.startsWith("c")) {
                        return item.toUpperCase();
                    } else {
                        return item.toLowerCase();
                    }
                })
                .filter(item -> item.length() > 4)
                .forEach(item -> System.out.println(item));
        System.out.println(shoppingList);
        // error stream has already been operated upon or closed
        //stream.forEach(item -> System.out.println(item));

        List<String> newList = shoppingList.stream()
                .sorted()
                .map(item -> item.toUpperCase())
                .collect(Collectors.toList());
        System.out.println(newList);
    }
}
