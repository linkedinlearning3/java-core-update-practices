package lambdaandstreams.streams;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Planet {
    private String name;
    private double density;
    private boolean hasRings;
    private int numberOfMoons;

    public Planet(String name) {
        this.name = name;
    }
}
