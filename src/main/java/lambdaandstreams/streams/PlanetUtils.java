package lambdaandstreams.streams;

import java.util.List;
import java.util.stream.Collectors;

public class PlanetUtils {

    public List<Planet> getPlanetsBeginningWithM(List<Planet> planets) {
        return planets.stream()
                .filter(p -> p.getName().startsWith("M"))
                .collect(Collectors.toList());
    }

    public List<Planet> getPlanetsWithRings(List<Planet> planets) {
        return planets.stream()
//                .filter(p -> p.getName().equals("Jupiter")
//                        || p.getName().equals("Saturn")
//                        || p.getName().equals("Uranus")
//                        || p.getName().equals("Neptune")
//                )
                .filter(p -> p.isHasRings())
                .collect(Collectors.toList());
    }

    public List<Planet> getPlanetsWithMoreThanThreeMoons(List<Planet> planets) {
        return planets.stream()
//                .filter(p -> p.getName().equals("Jupiter")
//                        || p.getName().equals("Saturn")
//                        || p.getName().equals("Uranus")
//                        || p.getName().equals("Neptune")
//                )
                .filter(p -> p.getNumberOfMoons() >= 3)
                .collect(Collectors.toList());
    }

    public List<Planet> getPlanetsWithDensityGreaterThanFive(List<Planet> planets) {
        return planets.stream()
                .filter(p -> p.getName().equals("Mercury")
                        || p.getName().equals("Venus")
                        || p.getName().equals("Earth")
                )
                .collect(Collectors.toList());
    }

}