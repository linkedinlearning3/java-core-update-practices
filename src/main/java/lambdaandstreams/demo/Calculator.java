package lambdaandstreams.demo;

@FunctionalInterface
public interface Calculator {
    int calculate(int a, int b);
}
