package lambdaandstreams.demo;

@FunctionalInterface
public interface Greeting {
    void sayHello();
}
