package lambdaandstreams.demo;

import java.util.Random;
import java.util.function.IntBinaryOperator;

public class Main {
    public static void main(String[] args) {
        GreetingImpl greeting1 = new GreetingImpl();
        greeting1.sayHello();

        Greeting greeting2 = new Greeting() {
            @Override
            public void sayHello() {
                System.out.println("Say Hello 2");
            }
        };
        greeting2.sayHello();

        // using java 8 lambda
        Greeting greeting3 = () -> System.out.println("Say Hello 3");
        greeting3.sayHello();


        Calculator calculator = (int a, int b) -> {
            Random random = new Random();
            int randomNumber = random.nextInt(51); // 0 -> 50
            return a * b + randomNumber;
        };
        System.out.println(calculator.calculate(9, 19));

        // built in from java
        IntBinaryOperator calculator2 = (int a, int b) -> {
            Random random = new Random();
            int randomNumber = random.nextInt(51); // 0 -> 50
            return a * b + randomNumber;
        };
        System.out.println(calculator2.applyAsInt(9, 19));
    }
}
